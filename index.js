
//3. Single room
db.hotel.insertOne(
    
    {
    name: "single",
    accommodates: 3,
    price: 1000,
    description: "A simple room with all the basic necessities",
    rooms_available: 10,
    isAvailable: false,
    }
)

//4. Multiple rooms
db.hotel.insertMany([
    
    {
        name: "double",
        accommodates: 3,
        price: 2000,
        description: "A room fit for a small family goin on a vacation",
        rooms_available: 5,
        isAvailable: false,
    },
    {
        name: "queen",
        accommodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway",
        rooms_available: 15,
        isAvailable: false,    
    }
])

//5. find double
db.hotel.find({name: "double"})

//6. update queen room rooms_available to 0
db.hotel.updateOne({name: "queen"},{$set:{rooms_available: 0}})

//7. Delete room with rooms_available is 0
db.hotel.deleteMany({rooms_available: 0})